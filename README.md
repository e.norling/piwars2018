# piwars2018
This is a repository for our piwars 2018, for Glitterator 2. At the moment, glitterator 2 has two pis: a pi zero w to drive the motors and handle the wireless controller, and a pi 3, which communicates with the pi zero via serial, to do the sensing and send autonomous control to the motors on the pi zero w.
