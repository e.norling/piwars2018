# Initial code for colour detection in the "Over the Rainbow" challenge.
# Adapted code from this tutorial:
# https://thecodacus.com/opencv-object-tracking-colour-detection-python/#.WmSOCSOcbBI
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
import numpy as np
import argparse
import time

# Get the LED strip going
from neopixel import *
# LED strip configuration:
LED_COUNT      = 8      # Number of LED pixels.
LED_PIN        = 21      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0
LED_STRIP      = ws.SK6812W_STRIP

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
# Intialize the library (must be called once before other functions).
strip.begin()
for i in range(strip.numPixels()):
    strip.setPixelColor(i, Color(255, 255, 255, 255))
    strip.show()


# Bounds for different colours (as (lower,upper) bounds for HSV values)
green=(np.array([33,80,40]),np.array([102,255,255]))
# Need two masks for red as it falls at both ends of the hue range
red1=(np.array([0,70,50]), np.array([10,255,255]))
red2=(np.array([170, 70, 50]), np.array([180, 255, 255]))
blue=(np.array([110,50,50]), np.array([130,255,255]))
# These values probably need tweaking, as doesn't detect yellow well in low light
yellow=(np.array([16,128,128]),np.array([40,255,255]))


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser(description='Test colour identification', epilog='Uses camera by default, or test image (specified with --image flag) otherwise')
ap.add_argument("-i", "--image", help = "path to the image")
args = vars(ap.parse_args())

print(args)

cam= cv2.VideoCapture(0)
kernelOpen=np.ones((5,5))
kernelClose=np.ones((20,20))

fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 1

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (320, 240)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(320, 240))
 
# allow the camera to warmup
time.sleep(0.1)
 
# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # grab the raw NumPy array representing the image, then initialize the timestamp
    # and occupied/unoccupied text
    img = frame.array

    #convert BGR to HSV
    imgHSV= cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

    greenMask=cv2.inRange(imgHSV,green[0],green[1])
    greenMaskOpen=cv2.morphologyEx(greenMask,cv2.MORPH_OPEN,kernelOpen)
    greenMaskClose=cv2.morphologyEx(greenMaskOpen,cv2.MORPH_CLOSE,kernelClose)
    greenImg = img.copy()
    greenMaskFinal=greenMaskClose
    _,conts,h=cv2.findContours(greenMaskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(greenImg,conts,-1,(255,0,0),3)
    cv2.imshow("green",greenImg)

    redImg = img.copy()
    redMask=cv2.inRange(imgHSV,red1[0],red1[1])+cv2.inRange(imgHSV,red2[0],red2[1])
    redMaskOpen=cv2.morphologyEx(redMask,cv2.MORPH_OPEN,kernelOpen)
    redMaskClose=cv2.morphologyEx(redMaskOpen,cv2.MORPH_CLOSE,kernelClose)
    redMaskFinal=redMaskClose
    _,conts,h=cv2.findContours(redMaskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(redImg,conts,-1,(255,0,0),3)
    cv2.imshow("red",redImg)

    blueImg = img.copy()
    blueMask=cv2.inRange(imgHSV,blue[0],blue[1])
    blueMaskOpen=cv2.morphologyEx(blueMask,cv2.MORPH_OPEN,kernelOpen)
    blueMaskClose=cv2.morphologyEx(blueMaskOpen,cv2.MORPH_CLOSE,kernelClose)
    blueMaskFinal=blueMaskClose
    _,conts,h=cv2.findContours(blueMaskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(blueImg,conts,-1,(255,0,0),3)
    cv2.imshow("blue",blueImg)

    yellowImg = img.copy()
    yellowMask=cv2.inRange(imgHSV,yellow[0],yellow[1])
    yellowMaskOpen=cv2.morphologyEx(yellowMask,cv2.MORPH_OPEN,kernelOpen)
    yellowMaskClose=cv2.morphologyEx(yellowMaskOpen,cv2.MORPH_CLOSE,kernelClose)
    yellowMaskFinal=yellowMaskClose
    _,conts,h=cv2.findContours(yellowMaskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(yellowImg,conts,-1,(255,0,0),3)
    cv2.imshow("yellow",yellowImg)

    key = cv2.waitKey(10)
    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)
 
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(0, 0, 0, 0))
            strip.show()
        break
