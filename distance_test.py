import time
import sys
import VL53L0X
import RPi.GPIO as GPIO

xshut_pin = 9

try:
    tof = VL53L0X.VL53L0X()
    print ("Created tof sensor")
    timing = tof.get_timing()/1000
    if (timing < TIMING):
        timing = TIMING
    print ("Timing %d ms" % (timing))

    # Make sure we can get a proper reading from the sensor
    distance = tof.get_distance()
    print ("Distance measurement from sensor %d is %d"
               % (tof.my_object_number, distance))

    while(True):
        time.sleep(1)
        print ("Distance measurement from sensor %d is %d"
               % (tof.my_object_number, distance))
           
except KeyboardInterrupt:
    GPIO.cleanup()
