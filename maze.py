import time
import sys
import serial
import VL53L0X
import Adafruit_LSM303

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

ser = serial.Serial(
	port='/dev/ttyAMA0',
	baudrate = 9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=1
	)
lsm303 = Adafruit_LSM303.LSM303()

mx = Motors(100, 1, 2, 3)

WIDTH = 350         # distance between walls (mm)
LEFT_DISTANCE = 80  # Want to keep wall roughly this far away on LHS (mm)
MARGIN = 10         # amount allowed to stray from desired distance from wall (mm)
START_SPEED = 40    # Start from this speed (%)
MAX_SPEED = 100     # Never go faster than this speed (%)
SPEED_INC = 0.05    # Rate at which to increase speed
MAX_INCS = 9        
SENSITIVITY = 5     # measurement sensitivity (5mm)
MEGA_SENSITIVITY = 20
SENSOR_SPACE = 100  # distance (mm) between sensors
TURN_SPEED = 40     # speed at which to turn (one side goes - this to turn on spot) (%)
FAST_TURN = 65
SLOW_TURN = 25
TIMING = 100        # min time between measurements (ms)
MAX_READ = 8190     # largest reading from sensors
FRONT_MARGIN = 1.9*LEFT_DISTANCE

# GPIO pins for Sensor shutdown pins
sensor_front_shutdown = 17
sensor_left_shutdown = 27
sensor_back_shutdown = 22
sensor_right_shutdown = 10

def setup():
    global front_tof, left_tof, back_tof, right_tof
    # Setup GPIO for shutdown pins on each VL53L0X
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(sensor_front_shutdown, GPIO.OUT)
    GPIO.setup(sensor_left_shutdown, GPIO.OUT)
    GPIO.setup(sensor_back_shutdown, GPIO.OUT)
    GPIO.setup(sensor_right_shutdown, GPIO.OUT)

    # Set all shutdown pins low to turn off each VL53L0X
    GPIO.output(sensor_front_shutdown, GPIO.LOW)
    GPIO.output(sensor_left_shutdown, GPIO.LOW)
    GPIO.output(sensor_back_shutdown, GPIO.LOW)
    GPIO.output(sensor_right_shutdown, GPIO.LOW)

    # Keep all low for 500 ms or so to make sure they reset
    time.sleep(0.50)

    # Create one object per VL53L0X passing the address to give to
    # each.
    front_tof = VL53L0X.VL53L0X(address=0x2B)
    left_tof = VL53L0X.VL53L0X(address=0x2D)
    back_tof = VL53L0X.VL53L0X(address=0x2F)
    right_tof = VL53L0X.VL53L0X(address=0x31)

    # Set shutdown pin high for the first VL53L0X then 
    # call to start ranging 
    GPIO.output(sensor_front_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_left_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_back_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_right_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    right_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    timing = front_tof.get_timing()
    if (timing < 20000):
        timing = 20000
    print ("Timing %d ms" % (timing/1000))

def go_button(channel):
    """Callback button for after robot is ready to run.
    Will start/stop the robot.
    """
    global should_go, left_speed, right_speed
    should_go = not should_go
    if (should_go):
        print "***Starting the maze"
        # start relatively slowly; jumping straight to max speed makes us turn
        left_speed = START_SPEED
        right_speed = START_SPEED
        direction = STRAIGHT
        check_position()
    else:
        print "***Stopping"
        left_speed = 0
        right_speed = 0
        mx.stop()


STRAIGHT = 1
LEFT = 2
ALIGNING = 3
def check_position():
    """Work out what direction we should be heading and set the
    two motor speeds accordingly.
    """
    global direction, front_dist, left_dist, right_dist, back_dist
    global left_speed, right_speed, cleared

    prev_max = max(left_speed, right_speed, START_SPEED)
    print ("Prev max was %d" % (prev_max))
    prev_front = front_dist
    prev_left = left_dist
    prev_right = right_dist
    prev_back = back_dist

    front_distance = front_tof.get_distance()
    if (front_dist == MAX_READ):
            front_distance = front_tof.get_distance()
            
    left_dist = left_tof.get_distance()
    if (left_dist == MAX_READ):
        left_dist = lf_tof.get_distance()

    right_dist = right_tof.get_distance()
    if (right_dist == MAX_READ):
        right_dist = right_tof.get_distance()

    print ("front is %dmm from wall, left  is %dmm, right is %dmm, back is %dmm"
            % (front_dist, left_dist, right_dist, back_dist))
    
    if (left_dist > WIDTH and not (front_distance == MAX_READ) and direction == STRAIGHT):
        # Need to get around the sticky-out bit
        direction = LEFT
        cleared = False
    if (direction == LEFT):
        print "turning left"
        turn_left(front_distance, left_dist, right_dist, prev_max)
    elif (direction == STRAIGHT):
        if (front_distance < FRONT_MARGIN
              or (front_distance < WIDTH and left_dist < LEFT_DISTANCE-MARGIN)):
            # need to stop and change direction to follow the wall currently in front
            if (left_speed > 0 or right_speed > 0):
                print "stop"
                left_speed = 0
                right_speed = 0
            else:
                print "turn right"
                left_speed = TURN_SPEED
                right_speed = -TURN_SPEED
                mx.drive(left_speed, right_speed)
        else:
            go_straight(front_distance, left_dist, right_dist, prev_max)

def turn_left(front_distance, left_dist, right_dist, prev_max):
    global left_speed, right_speed
    global direction, cleared

    # turn until left front can see wall on left
    if (left_dist < LEFT_DISTANCE+MARGIN):
        # reached target
        left_speed = 0
        right_speed = 0
        direction = STRAIGHT
        print "Reached target"
        #GPIO.cleanup()
        #sys.exit()
    else:
        left_speed = SLOW_TURN
        right_speed = FAST_TURN
        print "Turning left"
        
def go_straight(front_distance,left_dist, right_dist, prev_max):
    global left_speed, right_speed

    # Should turn more sharply the closer we are to a wall 
    if (left_dist < LEFT_DISTANCE-MARGIN):
        # Too close to the wall; move back to comfort zone
        diff = left_dist - right_dist
        if (diff > SENSITIVITY):
            left_speed = prev_max
            right_speed = prev_max
            print ("Too close to wall but already heading right")
        else:
            factor = abs(diff)/10.0
            print ("factor is %f"%(factor))
            if (factor > 1):
                factor = 1
            factor = int(factor*MAX_INCS)+1
            left_speed = (1+factor*SPEED_INC)*START_SPEED
            right_speed = START_SPEED
            print ("Too close, turning right")
    elif (left_dist > LEFT_DISTANCE+MARGIN):
        # Too far from wall; move back to comfort zone
        diff = right_dist - left_dist
        if (diff > SENSITIVITY and right_dist < WIDTH):
            left_speed = prev_max
            right_speed = prev_max
            print("Too far from wall, but already heading left")
        else:
            if (left_dist > 2*LEFT_DISTANCE):
                if (left_speed > 0 or right_speed > 0):
                    print "stop"
                    left_speed = 0
                    right_speed = 0
                else:
                    print "turn left"
                    left_speed = -TURN_SPEED
                    right_speed = TURN_SPEED
                    mx.drive(left_speed, right_speed)
            else:
                factor = abs(diff)/10.0
                print ("factor is %f"%(factor))
                if (factor > 1):
                    factor = 1
                factor = int(factor*MAX_INCS)+1
                left_speed = START_SPEED
                right_speed = (1+factor*SPEED_INC)*START_SPEED
            print ("Too far, turning left")
    else:
        straighten_up(front_distance, left_dist, right_dist, prev_max)

def straighten_up(front_distance, left_dist, right_dist, prev_max):
    global left_speed, right_speed

    # In comfort zone; aim to head straight
    diff = abs(left_dist-right_dist)
    # since we're in comfort zone, speed up (gradually) if we can
    print ("Difference between sensor readings is %d" % (diff))

    if (diff > SENSITIVITY):
        if (left_dist > right_dist):
            # currently heading right
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*left_speed
            if (slower > right_speed):
                left_speed = slower
        else:
            # currently heading left
            if (left_speed == 0):
                left_speed = START_SPEED
                right_speed = START_SPEED
            slower = (1-SPEED_INC)*right_speed
            if (slower > left_speed):
                right_speed = slower
        print "In comfort zone, straightening up"
    else:
        max_speed = MAX_SPEED
        if (front_distance < WIDTH):
            max_speed /=2
        speed = min(max_speed, (1+SPEED_INC)*prev_max)
        print ("New max speed is %f" %(speed))
        left_speed = speed
        right_speed = speed
        print "In comfort zone, speeding up and heading straight"

                
try:
    setup()
    
    # All set up, enter driving loop
    # Assume set up to head straight to start
    direction = STRAIGHT
    padding = IntervalCheck(interval=timing/1000.0)
    while(True):
        with padding:
            if (should_go):
                check_position()
                print ("Motor left %.2f motor right %.2f"
                        % (left_speed, right_speed))
                mx.drive(left_speed, right_speed)

except KeyboardInterrupt:
    if not c is None:
        c.close()
    mx.stop()
    GPIO.cleanup()

