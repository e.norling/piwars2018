#!/usr/bin/python

# MIT License
# 
# Copyright (c) 2017 John Bryan Moore
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import time
import math
import serial
import VL53L0X
import Adafruit_LSM303
import RPi.GPIO as GPIO

ser = serial.Serial(
	port='/dev/ttyAMA0',
	baudrate = 9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=1
	)
lsm303 = Adafruit_LSM303.LSM303()

# GPIO for Sensor 1 shutdown pin
sensor1_shutdown = 17
# GPIO for Sensor 2 shutdown pin
sensor2_shutdown = 27
sensor3_shutdown = 22
sensor4_shutdown = 10

GPIO.setwarnings(False)

# Setup GPIO for shutdown pins on each VL53L0X
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor1_shutdown, GPIO.OUT)
GPIO.setup(sensor2_shutdown, GPIO.OUT)
GPIO.setup(sensor3_shutdown, GPIO.OUT)
GPIO.setup(sensor4_shutdown, GPIO.OUT)

# Set all shutdown pins low to turn off each VL53L0X
GPIO.output(sensor1_shutdown, GPIO.LOW)
GPIO.output(sensor2_shutdown, GPIO.LOW)
GPIO.output(sensor3_shutdown, GPIO.LOW)
GPIO.output(sensor4_shutdown, GPIO.LOW)

# Keep all low for 500 ms or so to make sure they reset
time.sleep(0.50)

# Create one object per VL53L0X passing the address to give to
# each.
tof1 = VL53L0X.VL53L0X(address=0x2B)
tof2 = VL53L0X.VL53L0X(address=0x2D)
tof3 = VL53L0X.VL53L0X(address=0x2F)
tof4 = VL53L0X.VL53L0X(address=0x31)

# Set shutdown pin high for the first VL53L0X then 
# call to start ranging 
GPIO.output(sensor1_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof1.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

# Set shutdown pin high for the second VL53L0X then 
# call to start ranging 
GPIO.output(sensor2_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof2.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

GPIO.output(sensor3_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof3.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

GPIO.output(sensor4_shutdown, GPIO.HIGH)
time.sleep(0.50)
tof4.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

timing = tof1.get_timing()
if (timing < 20000):
    timing = 20000
print ("Timing %d ms" % (timing/1000))

for count in range(1,101):
    distance1 = tof1.get_distance()
    if (distance1 > 0):
        print ("sensor %d - %d mm, %d cm, iteration %d" % (tof1.my_object_number, distance1, (distance1/10), count))
    else:
        print ("%d - Error" % tof1.my_object_number)

    distance2 = tof2.get_distance()
    if (distance2 > 0):
        print ("sensor %d - %d mm, %d cm, iteration %d" % (tof2.my_object_number, distance2, (distance2/10), count))
    else:
        print ("%d - Error" % tof2.my_object_number)

    distance3 = tof3.get_distance()
    if (distance3 > 0):
        print ("sensor %d - %d mm, %d cm, iteration %d" % (tof3.my_object_number, distance3, (distance3/10), count))
    else:
        print ("%d - Error" % tof3.my_object_number)

    distance4 = tof4.get_distance()
    if (distance4 > 0):
        print ("sensor %d - %d mm, %d cm, iteration %d" % (tof4.my_object_number, distance4, (distance4/10), count))
    else:
        print ("%d - Error" % tof4.my_object_number)

    accel, mag = lsm303.read()
    accel_x, accel_y, accel_z = accel
    mag_x, mag_z, mag_y = mag

    heading = (math.atan2(mag_y,mag_x) * 180) / math.pi;
    if heading < 0:
        heading = 360 + heading;

    ser.write(bytes('%d: distances (mm): %d %d %d %d; accel: (%d, %d, %d), heading: %d\n'
                    %(count, distance1, distance2, distance3, distance4, accel_x, accel_y, accel_z, heading),'UTF-8'))
        
    time.sleep(timing/1000000.00)

tof4.stop_ranging()
GPIO.output(sensor4_shutdown, GPIO.LOW)
tof3.stop_ranging()
GPIO.output(sensor3_shutdown, GPIO.LOW)
tof2.stop_ranging()
GPIO.output(sensor2_shutdown, GPIO.LOW)
tof1.stop_ranging()
GPIO.output(sensor1_shutdown, GPIO.LOW)

