#!/usr/bin/env python3
# Simple test to check that serial communication is working (read side)
# A lot of setting up to do on the pis before you can test this. (Details
# in the Glitterator 2 blog.)
      
import time
import serial
          
ser = serial.Serial(
               port='/dev/ttyAMA0',
               baudrate = 9600,
               parity=serial.PARITY_NONE,
               stopbits=serial.STOPBITS_ONE,
               bytesize=serial.EIGHTBITS,
               timeout=1
           )
counter=0

while 1:
	x=ser.readline()
	print(x)
