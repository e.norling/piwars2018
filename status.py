import threading
import time
import blinkt

blinkt.set_clear_on_exit()

# Modes to display
STOPPED = 0
RUNNING = 1
LINE = 2
MAZE = 3
RAINBOW = 4
REMOTE = 5

class StatusBar():
    def __init__(self):
        self.status = STOPPED
        self.mode = None
        self.updateDisplay()

    def updateDisplay(self):
        if self.status == STOPPED:
            if self.mode is None:
                blinkt.set_all(255, 0, 0)
            elif self.mode == LINE:
                self.line()
            elif self.mode == MAZE:
                self.maze()
            elif self.mode == RAINBOW:
                self.rainbow()
            elif self.mode == REMOTE:
                self.remote()
        else:
            blinkt.set_all(0, 255, 0)
        blinkt.show()

    def setStatus(self, status):
        self.status = status
        self.updateDisplay()

    def setMode(self, mode):
        self.mode = mode
        self.updateDisplay()

    def line(self):
        blinkt.set_all(0, 0, 255)   # All blue

    def rainbow(self):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        blinkt.set_pixel(0, 148, 0, 211) # violet
        blinkt.set_pixel(1, 75, 0, 130)  # indigo
        blinkt.set_pixel(2, 0, 0, 255)   # blue
        blinkt.set_pixel(3, 0, 255, 255) # light blue
        blinkt.set_pixel(4, 0, 255, 0)   # green
        blinkt.set_pixel(5, 255, 255, 0) # yellow
        blinkt.set_pixel(6, 255, 127, 0) # orange
        blinkt.set_pixel(7, 255, 0, 0)   # red

    def maze(self):
        # Shades of pink
        blinkt.set_pixel(0, 255, 153, 255)
        blinkt.set_pixel(1, 255, 102, 255)
        blinkt.set_pixel(2, 255, 51, 255)
        blinkt.set_pixel(3, 255, 0, 255)
        blinkt.set_pixel(4, 204, 0, 204)
        blinkt.set_pixel(5, 153, 0, 153)
        blinkt.set_pixel(6, 102, 0, 102)
        blinkt.set_pixel(7, 51, 0, 51)    

    def remote(self):
        # Shades of grey
        blinkt.set_pixel(0, 224, 224, 224)
        blinkt.set_pixel(1, 192, 192, 192)
        blinkt.set_pixel(2, 160, 160, 160)
        blinkt.set_pixel(3, 128, 128, 128)
        blinkt.set_pixel(4, 96, 96, 96)
        blinkt.set_pixel(5, 64, 64, 64)
        blinkt.set_pixel(6, 32, 32, 32)
        blinkt.set_pixel(7, 0, 0, 0)     

# Main program logic follows:
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    status = StatusBar()

    while True:
        print('line')
        status.setMode(LINE)
        time.sleep(2)
        print('maze')
        status.setMode(MAZE)
        time.sleep(2)
        print('rainbow')
        status.setMode(RAINBOW)
        time.sleep(2)
        print('running')
        status.setStatus(RUNNING)
        time.sleep(2)
        print('stopped')
        status.setStatus(STOPPED)
        status.setMode(None)
        time.sleep(2)
