import time
import math
import sys
import serial
import VL53L0X
from util import IntervalCheck

import RPi.GPIO as GPIO
GPIO.setwarnings(False)

ser = serial.Serial(
	port='/dev/ttyAMA0',
	baudrate = 9600,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=1
	)

# left motor goes faster than right, use this factor
# to even them out
LEFT_THROTTLE = 0.95
RIGHT_THROTTLE = 1

CORRIDOR_WIDTH = 540 # distance between the walls (mm)
ROBOT_WIDTH = 210   # width between left and right sensors of robot
MIN_DIST = 100        # Comfortable distance from wall (mm)
MIN_ANGLE = math.pi/30 # Min angle to worry about (5 degrees, in rad)
FRONT_CLEARANCE = ROBOT_WIDTH # don't get closer to wall in front than this
MID = (CORRIDOR_WIDTH-ROBOT_WIDTH)/2
print('Mid is %f'%(MID))
MIN_SPEED = 40      # Start from this speed (%)
MAX_SPEED = 100     # Never go faster than this speed (%)
MAX_TURN = 20       # Max amount to add to min speed on outer wheel for a turn

SPACING = 75        # spacing between 2 sensors on left side (mm)
SENSOR_TO_FRONT_L = 105 # Distance from left mid side sensor to front of tracks (mm)
SENSOR_TO_FRONT_R = 85 # Distance from right side sensor to front of tracks (mm)
PI_ON_2 = math.pi/2

INC = 0.05
EPSILON_H = 5       # Heading seems to be tolerant to about 5 degrees
EPSILON_D = 2       # Distance sensors tolerant to about 2mm
MAX_ANGLE = math.pi/6 # 30 degress (in radians)


# GPIO pins for Sensor shutdown pins
sensor_front_shutdown = 17
sensor_left_shutdown = 27
sensor_right_shutdown = 22
sensor_back_shutdown = 10
sensor_left_back_shutdown = 9

def get_distances():
    # Get readings from distance sensors
    left = left_tof.get_distance()
    front = front_tof.get_distance()
    right = right_tof.get_distance()
    back = back_tof.get_distance()
    left_back = left_back_tof.get_distance()
    while left < 0 and right < 0 and back < 0 and front < 0 and left_back < 0:
        print('All readings shot')
        time.sleep(0.05)
        left = left_tof.get_distance()
        front = front_tof.get_distance()
        right = right_tof.get_distance()
        back = back_tof.get_distance()
        left_back = left_back_tof.get_distance()
    return front, left, back, right, left_back
        
def setup():
    global front_tof, left_tof, right_tof, back_tof, left_back_tof
    global left_distance, right_distance, front_distance, back_distance, left_back_distance
    global timing, in_corridor
    # Setup GPIO for shutdown pins on each VL53L0X
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(sensor_front_shutdown, GPIO.OUT)
    GPIO.setup(sensor_left_shutdown, GPIO.OUT)
    GPIO.setup(sensor_right_shutdown, GPIO.OUT)
    GPIO.setup(sensor_back_shutdown, GPIO.OUT)
    GPIO.setup(sensor_left_back_shutdown, GPIO.OUT)

    # Set all shutdown pins low to turn off each VL53L0X
    GPIO.output(sensor_front_shutdown, GPIO.LOW)
    GPIO.output(sensor_left_shutdown, GPIO.LOW)
    GPIO.output(sensor_right_shutdown, GPIO.LOW)
    GPIO.output(sensor_back_shutdown, GPIO.LOW)
    GPIO.output(sensor_left_back_shutdown, GPIO.LOW)

    # Keep all low for 500 ms or so to make sure they reset
    time.sleep(0.50)

    # Create one object per VL53L0X passing the address to give to
    # each.
    front_tof = VL53L0X.VL53L0X(address=0x2B)
    left_tof = VL53L0X.VL53L0X(address=0x2D)
    right_tof = VL53L0X.VL53L0X(address=0x2F)
    back_tof = VL53L0X.VL53L0X(address=0x31)
    left_back_tof = VL53L0X.VL53L0X(address=0x33)

    # Set shutdown pin high for the first VL53L0X then 
    # call to start ranging 
    GPIO.output(sensor_front_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    front_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_left_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    left_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_right_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    right_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_back_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    GPIO.output(sensor_left_back_shutdown, GPIO.HIGH)
    time.sleep(0.50)
    left_back_tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

    timing = back_tof.get_timing()
    if (timing < 20000):
        timing = 20000
    print ("Timing %d ms" % (timing))

    # Get initial readings from distance sensors
    front_distance, left_distance, back_distance, right_distance, left_back_distance = get_distances()
    
    return left_distance > 0 and right_distance > 0 and front_distance > 0 and back_distance > 0 and left_back_distance > 0
        
def in_corridor():
    global front_distance
    if front_distance < FRONT_CLEARANCE:
        # Take another reading just to make sure it wasn't the robot rocking
        front_distance = front_tof.get_distance()
        if front_distance < FRONT_CLEARANCE:
            return False
    # Otherwise, make sure we still have walls on each side
    return left_distance < CORRIDOR_WIDTH and right_distance < CORRIDOR_WIDTH

def keep_centered():
    global left_distance, right_distance, front_distance, back_distance, left_back_distance
    global pw_left, pw_right
    front_distance, left_distance, back_distance, right_distance, left_back_distance = get_distances()

    print('f: %dmm l: %dmm b: %dmm r: %dmm; lb %d'
            % (front_distance, left_distance, back_distance, right_distance, left_back_distance))
    LEFT = 1
    RIGHT = 2
    STRAIGHT = 0
    direction = STRAIGHT
    diff = left_distance - left_back_distance
    angle = 0
    front = left_distance
    if diff < -EPSILON_D:
        print('Pointing left')
        direction = LEFT
        angle = math.atan2(left_back_distance-left_distance,SPACING)
        front = (left_distance*SPACING/(left_back_distance-left_distance)-SENSOR_TO_FRONT_L)*math.sin(angle)
    elif diff > EPSILON_D:
        print('Pointing right')
        direction = RIGHT
        angle = math.atan2(left_distance-left_back_distance,SPACING)
        front = (right_distance*SPACING/(left_distance-left_back_distance)-SENSOR_TO_FRONT_R)*math.sin(angle)
    else:
        angle = 0
        if left_distance < right_distance:
            front = left_distance
        else:
            front = right_distance
        print('Currently heading straight')

    print('Direction %f'%(math.degrees(angle)))
    angle_factor = min((angle)/MAX_ANGLE, 1)
    print('Angle factor is %f'%(angle_factor))
    print('Front is %f'%(front))
    distance_factor = (MID-front)/(MID)
    print('Distance factor is %f'%(distance_factor))
    factor = max(angle_factor, distance_factor)
    print('Overall factor is %f'%(factor))

    print('Speeds before adjustment %d %d'%(pw_left,pw_right))
    if direction == LEFT and front < MID:
        print('Heading in wrong direction near left wall')
        # Too far to the left, turn back to straight
        pw_right = MIN_SPEED
        pw_left = MIN_SPEED + factor*MAX_TURN

    elif direction == RIGHT and front < MID:
        print('Heading in wrong direction near right wall')
        # Too far to the left, turn back to straight
        pw_left = MIN_SPEED
        pw_right = MIN_SPEED + factor*MAX_TURN
        
    else:
        print('At least we are not heading in the wrong direction near a wall')
        # Want to end up heading straight along centre line
        if front < MIN_DIST:
            print('Too close to wall')
            if direction == STRAIGHT:
                # Heading straight but too close to wall
                print('Too close to wall (front: %f)'%(front))
                if left_distance < MIN_DIST:
                    pw_right = pw_right - factor*MAX_TURN
                else:
                    pw_left = pw_left - factor*MAX_TURN
            elif direction == LEFT:
                # Heading left but still to right of centre, near wall
                print('Heading good, keep going')
                if pw_right > pw_left:
                    pw_left = pw_right
            else:
                print('Heading good, keep going')
                # Heading right but still to left of center, near wall
                if pw_left > pw_right:
                    pw_right = pw_left
        elif front > MIN_DIST:
            # Far enough from wall, straighten up
            print('In safe zone, straighten up')
            if angle > MIN_ANGLE:
                if direction == RIGHT:
                    if pw_right < pw_left:
                        pw_right = pw_left
                    pw_left = pw_left * (1-INC)
                elif direction == LEFT:
                    if pw_left < pw_right:
                        pw_left = pw_right
                    pw_right = pw_right * (1-INC)
            else:
                pw_left = pw_left * (1+INC)
                pw_right = pw_right * (1+INC)

    # Keep within allowed bounds for speed
    pw_left = min(max(MIN_SPEED, pw_left),MAX_SPEED)
    pw_right = min(max(MIN_SPEED, pw_right),MAX_SPEED)

    print ('%d %d'% (pw_left, pw_right))
            
try:
    if not setup():
        print('Bad sensor reading')
        print('f: %dmm l: %dmm b: %dmm r: %dmm; bl: %d'
            % (front_distance, left_distance, back_distance, right_distance, left_back_distance))
        raise KeyboardInterrupt
    
    pw_left = MIN_SPEED
    pw_right = MIN_SPEED
    padding = IntervalCheck(interval=timing/1000000.0)
    print('Padding interval is %f' %(padding.interval))
    while in_corridor():
        with padding:
            keep_centered()
            ser.write(bytes("%d %d\n" % (int(pw_left*LEFT_THROTTLE), int(pw_right*RIGHT_THROTTLE)), 'UTF-8'))
            print("motors: %d %d\n"%(int(pw_left*LEFT_THROTTLE), int(pw_right*RIGHT_THROTTLE)))
    ser.write(bytes("%d %d\n" % (0,0), 'UTF-8'))

except KeyboardInterrupt:
    ser.write(bytes("%d %d\n" % (0,0), 'UTF-8'))
    GPIO.cleanup()
